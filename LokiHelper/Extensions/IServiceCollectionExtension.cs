using System;
using LokiHelper.AppConfig;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace LokiHelper.Extensions
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection ConfigureIdentity(this IServiceCollection serviceCollection,
            IdentityConfig identityConfig)
        {
            serviceCollection.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = identityConfig.Password.RequireDigit;
                options.Password.RequiredLength = identityConfig.Password.RequiredLength;
                options.Password.RequireLowercase = identityConfig.Password.RequireLowercase;
                options.Password.RequireNonAlphanumeric = identityConfig.Password.RequireNonAlphanumeric;
                options.Password.RequireUppercase = identityConfig.Password.RequireUppercase;

                options.Lockout.AllowedForNewUsers = identityConfig.Lockout.AllowedForNewUsers;
                options.Lockout.DefaultLockoutTimeSpan =
                    TimeSpan.FromMinutes(identityConfig.Lockout.DefaultLockoutTimeSpanInMins);
                options.Lockout.MaxFailedAccessAttempts = identityConfig.Lockout.MaxFailedAccessAttempts;

                options.User.RequireUniqueEmail = identityConfig.User.RequireUniqueEmail;

                options.SignIn.RequireConfirmedEmail = identityConfig.SignIn.RequireConfirmedEmail;
                options.SignIn.RequireConfirmedPhoneNumber = identityConfig.SignIn.RequireConfirmedPhoneNumber;

            });
            return serviceCollection;
        }
        
        public static IServiceCollection ConfigureCookie(this IServiceCollection serviceCollection,
            CookieConfig cookieConfig)
        {
            serviceCollection.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = cookieConfig.LoginPath;
                options.LogoutPath = cookieConfig.LogoutPath;
                options.AccessDeniedPath = cookieConfig.AccessDeniedPath;
                options.SlidingExpiration = cookieConfig.SlidingExpiration;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(cookieConfig.ExpireTimeSpanInMin);
                options.Validate();
            });
            return serviceCollection;
        }
    }
}