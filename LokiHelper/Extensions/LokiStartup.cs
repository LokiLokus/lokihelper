using System;
using Microsoft.Extensions.Configuration;

namespace LokiHelper.Extensions
{
    public abstract class LokiStartup
    {
        public IConfiguration Configuration { get; }
        
        public LokiStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public T GetRequiredConfig<T>(string section)
        {
            T setting = Configuration.GetSection(section).Get<T>();
            if (setting == null) throw new NullReferenceException(section + " is null");
            return setting;
        } 
        
        public T GetConfig<T>(string section)
        {
            T setting = Configuration.GetSection(section).Get<T>();
            return setting;
        }
    }
}