namespace LokiHelper.AppConfig
{
    public class DatabaseConfig {
        public string ConnectionString { get; set; }
        public string DatabaseTyp { get; set; }
    }
}